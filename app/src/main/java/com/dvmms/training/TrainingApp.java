package com.dvmms.training;

import android.app.Application;

import com.dvmms.training.core.di.ApplicationComponent;
import com.dvmms.training.core.di.DaggerApplicationComponent;

public class TrainingApp extends Application {
    private static ApplicationComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = DaggerApplicationComponent.builder().build();
    }

    public static ApplicationComponent getAppComponent() {
        return sAppComponent;
    }
}
