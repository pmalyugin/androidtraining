package com.dvmms.training.core.domain.executors;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class UiThread implements PostExecutionThread {
    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
