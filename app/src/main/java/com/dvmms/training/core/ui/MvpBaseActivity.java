package com.dvmms.training.core.ui;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.dvmms.training.TrainingApp;
import com.dvmms.training.core.di.ApplicationComponent;
import com.dvmms.training.core.di.ActivityComponent;
import com.dvmms.training.core.di.modules.ActivityModule;
import com.dvmms.training.core.di.HasComponent;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public abstract class MvpBaseActivity<C extends ActivityComponent>
        extends MvpAppCompatActivity
        implements HasComponent<C> {

    private C component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        component = buildComponent();
        setupDI(component);
        super.onCreate(savedInstanceState);
    }

    protected ApplicationComponent getApplicationComponent() {
        return TrainingApp.getAppComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    @Override
    public C getComponent() {
        return component;
    }

    //region DI
    protected abstract C buildComponent();
    protected abstract void setupDI(C component);
    //endregion
}