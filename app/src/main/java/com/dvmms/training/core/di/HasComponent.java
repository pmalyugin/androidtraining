package com.dvmms.training.core.di;

/**
 * Created by pmalyugin on 13/03/2018.
 */

/**
 * Interface representing a contract for clients that contains a component for dependency injection.
 */
public interface HasComponent<C> {
    C getComponent();
}
