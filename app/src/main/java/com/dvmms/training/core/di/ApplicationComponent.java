package com.dvmms.training.core.di;

import com.dvmms.training.core.di.modules.ExecutorsModule;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Singleton
@Component(
        modules = {
                ExecutorsModule.class
        }
)
public interface ApplicationComponent {
}
