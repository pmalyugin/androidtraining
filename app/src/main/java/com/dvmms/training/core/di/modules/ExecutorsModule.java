package com.dvmms.training.core.di.modules;

import com.dvmms.training.core.domain.executors.JobExecutor;
import com.dvmms.training.core.domain.executors.UiThread;
import com.dvmms.training.core.domain.executors.PostExecutionThread;
import com.dvmms.training.core.domain.executors.ThreadExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Module
public class ExecutorsModule {
    @Singleton
    @Provides
    ThreadExecutor provideThreadExecutor() {
        return new JobExecutor();
    }

    @Singleton
    @Provides
    PostExecutionThread providePostExecutionThread() {
        return new UiThread();
    }
}
