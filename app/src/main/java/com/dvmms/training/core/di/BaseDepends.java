package com.dvmms.training.core.di;

import com.dvmms.training.core.domain.executors.PostExecutionThread;
import com.dvmms.training.core.domain.executors.ThreadExecutor;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface BaseDepends {
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
}
